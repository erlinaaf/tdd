package com.gojek;

import org.junit.Test;

import static org.junit.Assert.*;

public class listTest {
    @Test
    public void getTaskByIDTest(){
        list taskList = new list();
        String expectedTask = "1. Do dishes [DONE]";

        String actual = taskList.getTaskByID(1);

        assertEquals(expectedTask, actual);
    }

    @Test
    public void getAllTaskTest(){
        list taskList = new list();
        String expectedTask = "1. Do dishes [DONE]\n2. Learn Java [NOT DONE]\n3. Learn TDD [NOT DONE]";

        String actual = taskList.getAllTask();

        assertEquals(expectedTask, actual);
    }




}