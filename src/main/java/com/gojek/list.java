package com.gojek;

import java.util.ArrayList;

public class list {
    private ArrayList<task> tasklist;

    public list(){
        this.tasklist = new ArrayList<>();
        this.tasklist.add(new task(1, "Do dishes", "[DONE]"));
        this.tasklist.add(new task(2, "Learn Java", "[NOT DONE]"));
        this.tasklist.add(new task(3, "Learn TDD", "[NOT DONE]"));
    }

    public String getTaskByID(int id){
        for(task Task: tasklist){
            if(Task.getId() == id){
                return Task.getId() + ". " + Task.getName() + " " + Task.getStatus();
            }
        }
        return null;
    }

    public String getAllTask(){
        String allTask = "";
        int length = tasklist.size();
        for(int i=0; i<length; i++)
        {
            task Task = tasklist.get(i);
            if(i == length-1){
                allTask+=Task.getId()+". "+ Task.getName() + " " + Task.getStatus();
            } else{
                allTask+=Task.getId()+". "+ Task.getName() + " " + Task.getStatus() + "\n";
            }
        }
        return allTask;
    }
}
